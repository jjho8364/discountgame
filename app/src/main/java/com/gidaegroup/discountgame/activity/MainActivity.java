package com.gidaegroup.discountgame.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;

import com.gidaegroup.discountgame.R;
import com.gidaegroup.discountgame.adapter.GridViewAdapter;
import com.gidaegroup.discountgame.item.GridviewItem;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {
    private final String TAG = " MainActivity - ";
    private WebView webView;
    private String baseUrl = "https://play.google.com/store/apps/collection/promotion_3002c9b_gamesDynamic_cyberweek2017?clp=CjQKMgoscHJvbW90aW9uXzMwMDJjOWJfZ2FtZXNEeW5hbWljX2N5YmVyd2VlazIwMTcQBxgD:S:ANO1ljLDgW8&gsr=CjYKNAoyCixwcm9tb3Rpb25fMzAwMmM5Yl9nYW1lc0R5bmFtaWNfY3liZXJ3ZWVrMjAxNxAHGAM%3D:S:ANO1ljIdCJM";
    private Handler mHandler;
    private Handler gridViewHandler;
    private ProgressDialog mProgressDialog;

    private GridView gridView;
    ArrayList<GridviewItem> arrGridViewItem;
    ArrayList<GridviewItem> arrGridViewItemAdditional;

    private int firstListSize = 0;
    private boolean firstExecute = true;

    GridViewAdapter gridViewAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        mHandler = new Handler();
        gridView = (GridView)findViewById(R.id.gridview);

        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new MyJavascriptInterface(), "Android");
        String newUA= "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";
        webView.getSettings().setUserAgentString(newUA);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                return super.shouldInterceptRequest(view, url);
            }
            /////////////////////////////////////////////////////////////////////////////////////
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(TAG, "Processing webview url click..." + url);
                Log.d(TAG, "clicked webview2");
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(webView, url);
                Log.d(TAG, "onPageFinished..");

                // get additional list
                if(firstExecute){
                    firstExecute = false;
                    if(MainActivity.this != null) webView.loadUrl("javascript:window.Android.getHtml(document.getElementsByTagName('html')[0].innerHTML);");
                }

            }
        });

        webView.loadUrl(baseUrl);
        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;


            }
        });

    }

    public class MyJavascriptInterface {
        @JavascriptInterface
        public void getHtml(String html) { //위 자바스크립트가 호출되면 여기로 html이 반환됨
            Document doc = null;
            arrGridViewItem = new ArrayList<GridviewItem>();
            try {
                doc = Jsoup.parse(html);

                // img
                Elements lists = doc.select(".FjwTrf");
                Log.d(TAG, "lists size : " + lists.size());
                firstListSize = lists.size();
                for(int i=0 ; i<lists.size() ; i++) {
                    if(lists.get(i).select(".LCATme span:").size() < 2) continue;   // already finish discount

                    String title = lists.get(i).select(".kCSSQe").text()+"";
                    String beforeDiscount = lists.get(i).select(".LCATme span:eq(0)").text()+"";
                    String afterDiscount = lists.get(i).select(".LCATme span:eq(1)").text()+"";
                    String imgUrl = lists.get(i).select(".yNWQ8e.K3IMke.buPxGf img").attr("data-src")+"";
                    String detailUrl = "https://play.google.com" + lists.get(i).select(".vU6FJ.p63iDd a").attr("href")+"";

                    GridviewItem item = new GridviewItem(title,beforeDiscount, afterDiscount, imgUrl, detailUrl);
                    arrGridViewItem.add(item);
                    //Log.d(TAG, imgUrl);
                }

                MyRunnable obj = new MyRunnable("11");
                mHandler.post(obj);

            } catch(Exception e){
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void getAddedHtml(String html) {
            Document doc = null;
            Log.d(TAG, "called getAddedHtml");

            try {
                doc = Jsoup.parse(html);

                // img
                Elements lists = doc.select(".Vpfmgd");
                Log.d(TAG, "additional lists size : " + lists.size());

                for(int i=firstListSize+1 ; i<lists.size() ; i++) {
                    if(lists.get(i).select(".LCATme span:").size() < 2) continue;   // already finish discount

                    String title = lists.get(i).select(".vU6FJ.p63iDd").text()+"";
                    String beforeDiscount = lists.get(i).select(".LCATme span:eq(0)").text()+"";
                    String afterDiscount = lists.get(i).select(".LCATme span:eq(1)").text()+"";
                    String imgUrl = lists.get(i).select(".N9c7d.eJxoSc img").attr("src")+"";
                    String detailUrl = "https://play.google.com" + lists.get(i).select(".vU6FJ.p63iDd a").attr("href")+"";

                    GridviewItem item = new GridviewItem(title, beforeDiscount, afterDiscount, imgUrl, detailUrl);
                    gridViewAdapter.addItem(item);

                    //Log.d(TAG, imgUrl);
                }

            } catch(Exception e){
                e.printStackTrace();
            }
        }

    }

    private Runnable mScrollDown = new Runnable() {
        public void run() {
            WebView webview = (WebView)findViewById(R.id.webView);
            webview.scrollBy(0, 1000);
            mHandler.postDelayed(this, 200);
        }
    };

    public class MyRunnable implements Runnable {
        private String imgHtml;
        public MyRunnable(String imgHtml) {
            this.imgHtml = imgHtml;
        }

        @Override
        public void run() {
            if(MainActivity.this != null){
                if(imgHtml.equals("11")){
                    Log.d(TAG, "set adapter");
                    gridViewAdapter = new GridViewAdapter(MainActivity.this, arrGridViewItem, R.layout.item_grid_category);
                    gridView.setAdapter(gridViewAdapter);
                    mScrollDown.run();

                    // in 5 sec call javascript

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(MainActivity.this != null) webView.loadUrl("javascript:window.Android.getAddedHtml(document.getElementsByTagName('html')[0].innerHTML);");
                        }
                    }, 5000);
                } else if(imgHtml.equals("22")) {

                }
            }
        }
    }
}
