package com.gidaegroup.discountgame.item;

public class GridviewItem {
    String title;
    String beforeDiscount;
    String afterDiscount;
    String imgUrl;
    String detailUrl;

    public GridviewItem(String title, String beforeDiscount, String afterDiscount, String imgUrl, String detailUrl) {
        this.title = title;
        this.beforeDiscount = beforeDiscount;
        this.afterDiscount = afterDiscount;
        this.imgUrl = imgUrl;
        this.detailUrl = detailUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBeforeDiscount() {
        return beforeDiscount;
    }

    public void setBeforeDiscount(String beforeDiscount) {
        this.beforeDiscount = beforeDiscount;
    }

    public String getAfterDiscount() {
        return afterDiscount;
    }

    public void setAfterDiscount(String afterDiscount) {
        this.afterDiscount = afterDiscount;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }
}
