package com.gidaegroup.discountgame.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gidaegroup.discountgame.R;
import com.gidaegroup.discountgame.item.GridviewItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GridViewAdapter extends BaseAdapter {
    Activity context;
    ArrayList<GridviewItem> gridArr;
    LayoutInflater inf;
    int layout;

    private ArrayList<GridviewItem> arraylist;

    public GridViewAdapter(Activity context, ArrayList<GridviewItem> gridArr, int layout) {
        this.context = context;
        this.gridArr = gridArr;
        this.inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layout = layout;
        this.arraylist = new ArrayList<GridviewItem>();
        arraylist.addAll(gridArr);
    }

    static class ViewHolder {
        public ImageView imageView;
        public TextView title;
        public TextView beforeDiscount;
        public TextView afterDiscount;
        public TextView starPoint;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (convertView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inf.inflate(layout, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageView= (ImageView)rowView.findViewById(R.id.gridview_img);
            viewHolder.title = (TextView)rowView.findViewById(R.id.gridview_title);
            viewHolder.beforeDiscount = (TextView)rowView.findViewById(R.id.gridview_before_discount);
            viewHolder.beforeDiscount.setPaintFlags(viewHolder.beforeDiscount.getPaintFlags()|Paint.STRIKE_THRU_TEXT_FLAG); // 사선 긋기
            viewHolder.afterDiscount = (TextView)rowView.findViewById(R.id.gridview_after_discount);
            viewHolder.starPoint = (TextView)rowView.findViewById(R.id.gridview_star_point);

            rowView.setTag(viewHolder);
        }




        ViewHolder holder = (ViewHolder)rowView.getTag();
        GridviewItem data = gridArr.get(position);
        if(data.getImgUrl()==null || data.getImgUrl().equals("")){
            holder.imageView.setImageResource(R.drawable.noimage);
        } else {
            Picasso.with(context).load(data.getImgUrl()).into(holder.imageView);
        }
        holder.title.setText(data.getTitle());
        holder.beforeDiscount.setText(data.getBeforeDiscount());
        holder.afterDiscount.setText(data.getAfterDiscount());
        holder.starPoint.setText("4.7점");

        return rowView;
    }

    @Override
    public int getCount() {
        return gridArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void addItem(GridviewItem item){
        gridArr.add(item);
    }

}
